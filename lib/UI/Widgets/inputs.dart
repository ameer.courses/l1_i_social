import 'package:flutter/material.dart';

class MyTextField extends StatefulWidget {


  final TextEditingController? controller;
  final String? hint;
  final Widget? icon;
  final bool isPass;
  final TextInputType? keyboardType;
  bool _hide = true;

  MyTextField({
    this.controller,
    this.hint,
    this.icon,
    this.keyboardType,
    this.isPass = false,
  });

  @override
  State<MyTextField> createState() => _MyTextFieldState();
}

class _MyTextFieldState extends State<MyTextField> {

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Padding(
      padding: EdgeInsets.all(size.width*0.025),
      child: TextField(
        // onChanged: (value){
        //   print(value);
        // },
        controller: widget.controller,
        decoration: InputDecoration(
            hintText: widget.hint,
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(size.width*0.025),
                borderSide: BorderSide.none
            ),
            filled: true,
            fillColor: Colors.grey[300],
            prefixIcon: widget.icon,
            suffixIcon: widget.isPass ?
            IconButton(
                onPressed: (){
                  setState((){
                    widget._hide = !widget._hide;
                  });
                },
                icon: widget._hide ?
                    Icon(Icons.visibility_off):
                    Icon(Icons.visibility)
            ) : null
        ),
        obscureText: widget.isPass && widget._hide,
        keyboardType: widget.keyboardType,
      ),
    );
  }
}
