import 'package:flutter/material.dart';
import 'package:social/UI/Widgets/inputs.dart';

class RegisterScreen extends StatelessWidget {

  static final TextEditingController emailController = TextEditingController();
  static final TextEditingController passController = TextEditingController();
  static final TextEditingController cpassController = TextEditingController();
  static final TextEditingController phoneController = TextEditingController();
  static final TextEditingController nameController = TextEditingController();


  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Register'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: size.width*0.45,
                height: size.width*0.45,
                margin: EdgeInsets.all(size.width*0.025),
                padding: EdgeInsets.all(size.width*0.075),
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(size.width*0.225),
                    color: Colors.grey[300]
                ),
                child: FittedBox(
                  child: Icon(Icons.person,color: Colors.grey,),
                ),
              ),

              MyTextField(
                controller: emailController,
                hint: 'email',
                icon: Icon(Icons.email),
                keyboardType: TextInputType.emailAddress,
              ),
              MyTextField(
                controller: nameController,
                hint: 'name',
                icon: Icon(Icons.person),
                keyboardType: TextInputType.name,
              ),
              MyTextField(
                controller: phoneController,
                hint: 'phone',
                icon: Icon(Icons.phone),
                keyboardType: TextInputType.phone,
              ),
              MyTextField(
                controller: passController,
                hint: 'password',
                icon: Icon(Icons.lock),
                isPass: true,
              ),
              MyTextField(
                controller: cpassController,
                hint: 'confirm password',
                icon: Icon(Icons.lock),
                isPass: true,
              ),
              SizedBox(
                height: size.height*0.05,
              ),
              ElevatedButton(
                  onPressed: (){
                    Navigator.pushReplacementNamed(context, '/home');
                  },
                  child: Text('Register')
              ),
              SizedBox(
                height: size.height*0.05,
              ),
              InkWell(
                child: Text('if you already have an account ,login.'),
                onTap: (){
                  Navigator.pushReplacementNamed(context, '/login');
                },
              ),
              SizedBox(
                height: size.height*0.05,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
