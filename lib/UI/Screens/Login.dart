import 'package:flutter/material.dart';
import 'package:social/UI/Widgets/inputs.dart';

class LoginScreen extends StatelessWidget {

  static final TextEditingController emailController = TextEditingController();
  final TextEditingController passController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      appBar: AppBar(
        title: Text('Login'),
      ),
      body: SizedBox(
        width: double.infinity,
        child: SingleChildScrollView(
          child: Column(
            children: [
              Container(
                width: size.width*0.45,
                height: size.width*0.45,
                margin: EdgeInsets.all(size.width*0.025),
                padding: EdgeInsets.all(size.width*0.075),
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(size.width*0.225),
                  color: Colors.grey[300]
                ),
                child: FittedBox(
                  child: Icon(Icons.person,color: Colors.grey,),
                ),
              ),

              MyTextField(
                controller: emailController,
                hint: 'email',
                icon: Icon(Icons.email),
                keyboardType: TextInputType.emailAddress,
              ),
              MyTextField(
                controller: passController,
                hint: 'password',
                icon: Icon(Icons.lock),
                isPass: true,

              ),
              SizedBox(
                height: size.height*0.05,
              ),
              ElevatedButton(
                  onPressed: (){
                    Navigator.pushReplacementNamed(context, '/home');
                  },
                  child: Text('Login')
              ),
              SizedBox(
                height: size.height*0.05,
              ),
              InkWell(
                  child: Text('if you don\'t have an account go to register.'),
                  onTap: (){
                    Navigator.pushReplacementNamed(context, '/register');
                  },
              ),
              SizedBox(
                height: size.height*0.05,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
