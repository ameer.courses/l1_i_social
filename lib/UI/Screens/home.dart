import 'package:flutter/material.dart';


class HomeScreen extends StatelessWidget {


  final List<String> names = [
    'ameer',
    'ali',
    'omar',
    'ameer',
    'ali',
    'omar',
    'ameer',
    'ali',
    'omar',
    'ameer',
    'ali',
    'omar',
    'ameer',
    'ali',
    'omar',
    'ameer',
    'ali',
    'omar',
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Home'),
      ),
      drawer: Drawer(
        child: ListView(
          children: [
            DrawerHeader(
              padding: EdgeInsets.zero,
              child: Container(
                color: Colors.blue,
              ),
            ),
            Card(
              child: ListTile(
                title: Text('Search'),
                subtitle: Text('tap to search'),
                leading: Icon(Icons.search),
                trailing: Icon(Icons.arrow_forward_ios),
              ),

            )
          ],
        ),
      ),
      // body: ListView.builder(
      //     itemCount: names.length,
      //     itemBuilder: (context,i) =>
      //       ListTile(
      //         title: Text(names[i]),
      //         leading: Icon(Icons.person),
      //       ),
      // ),
      // body: PageView(
      //   children: [
      //     Container(
      //       color: Colors.red,
      //     ),
      //     Container(
      //       color: Colors.teal,
      //     ),
      //   ],
      // ),
      body: Column(
        children: [
          Container(
            width: 150,
            height: 300,
            color: Colors.red,
          ),
          Expanded(
              child: LayoutBuilder(
            builder: (context,constrains){
              return Container(
                width: constrains.maxWidth*0.5,
                //height: constrains.maxHeight*0.5,
                color: Colors.blue,
              );
            },
          )
          )
        ],
      ),
    );
  }
}
