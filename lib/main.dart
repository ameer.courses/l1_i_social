import 'package:flutter/material.dart';
import 'package:social/UI/Screens/home.dart';
import 'package:social/UI/Screens/register.dart';

import 'UI/Screens/Login.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {

    return MaterialApp(
      routes: {
        '/login' : (c) => LoginScreen(),
        '/register' : (c) => RegisterScreen(),
        '/home' : (c) => HomeScreen()
      },
      theme: ThemeData.light().copyWith(
        appBarTheme: AppBarTheme(
          backgroundColor: Colors.white,
          foregroundColor: Colors.black,
          centerTitle: true,
        ),

      ),
      initialRoute: '/login',
    );
  }
}

